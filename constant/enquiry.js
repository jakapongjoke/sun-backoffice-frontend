export const enquiryheader = [
    {
    text: 'Enquiry No',
    value: 'enquiry_no',
    fieldname: 'enquiry_no',
    type: 'text',
    sortable: true,
    component: '',
    link: 'modal',
    onclickmethod: 'enquirydata'
},
    {
    text: 'Delivery No',
    value: 'delivery_no',
    fieldname: 'delivery_no',
    type: 'text',
    sortable: true,
    component: '',
    link: '',
    onclickmethod: ''
},
    {
    text: 'Delivery Date',
    value: 'delivery_date',
    fieldname: 'delivery_date',
    type: 'text',
    sortable: true,
    component: '',
    link: '',
    onclickmethod: ''
},
    {
    text: 'Customer',
    value: 'customer',
    fieldname: 'customer',
    type: 'text',
    sortable: true,
    component: '',
    link: '',
    onclickmethod: ''
},
    {
    text: 'Origin',
    value: 'origin',
    fieldname: 'origin',
    type: 'text',
    sortable: true,
    component: '',
    link: '',
    onclickmethod: ''
},
    {
    text: 'Destrination',
    value: 'destrination',
    fieldname: 'destrination',
    type: 'text',
    sortable: false,
    component: '',
    link: '',
    onclickmethod: ''
},
    {
    text: 'Status',
    value: 'Pending',
    fieldname: 'destrination',
    type: 'select',
    sortable: true,
    component: 'SelectOption',
    link: '',
    onclickmethod: ''
}

]

export const enquiryprops = {
    headers: {
      type: Array,
      default: null,
      required: true
    },
    data: {
      type: Array,
      default: null,
      required: true
    }
}

export const listdata = [
        {
          enquiry_no: 'EN2K1805001',
          delivery_no: 'DL2K1805001',
          delivery_date: '01/05/2018',
          customer: 'Client',
          origin: 'OrigiOsakan',
          destrination: 'OrigiOsakan'
        }
      ]
