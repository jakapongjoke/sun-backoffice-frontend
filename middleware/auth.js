import store from 'store'

export default function (context) {
   const token = context.app.$axios.get('http://localhost:3000/v1/token/checktoken', { headers: { 'Authorization': `Bearer ${store.get('token')}` } })
   token.then((token) => {
        return token
   })
}
