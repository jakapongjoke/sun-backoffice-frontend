import axios from '../index'
export default (username, password) => axios.post('http://localhost:3000/v1/user/login', {
  username,
  password
})
