export const state = () => ({
    is_login: false
})

export const actions = {
    login({ commit }) {
        commit('change_login', true)
    }
}
export const mutations = {
    change_login(state, status) {
     state.is_login = status
    }
}
export const getters = {
    get_is_login(state) {
        return state.is_login
    }
}
