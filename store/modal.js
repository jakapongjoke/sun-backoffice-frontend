export const state = () => ({
    is_shown: false
})

export const actions = {
    updateStatusAction({ commit }, status) {
        console.log('updateStatusAction')
        commit('updatestatus', status)
    }
}

export const mutations = {
    updatestatus(state, status) {
        state.is_shown = status
    }
}

export const getters = {
    is_shown(state) {
        return state.is_login
    }
}
